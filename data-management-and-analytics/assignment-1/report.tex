\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage[pdftex]{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage{url}
\usepackage{csquotes}
\usepackage{tocbibind}
\usepackage[numbers]{natbib}
\usepackage{sectsty}
\usepackage[table]{xcolor}
\usepackage{float}

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}
%\newcommand{\citeyear}[1]{\citeauthor{#1}~[\citeyear{#1}]}
\newcommand{\note}[1]{ {\small \color{red!80}{\bf Note:} #1 \normalsize} }

\title{
	\vspace{1in} 	
	\horrule{0.5pt} \\[0.4cm]
	\huge Supercomputing for Challenging Applications \\
	\huge \textit{Data management and analytics} \\
	{\Large Assignment 1} \\
	\horrule{2pt} \\
}
\author{
	\Large Sergi Granell Escalfet \\
	\\
	\today
}
\date{}

\begin{document}

\pagenumbering{gobble}

\maketitle

\newpage

\tableofcontents

\newpage

\pagenumbering{arabic}

\section{Why is Graph Management Challenging?}

As \citet{Junghanns2017} study on their article, nowadays there are many big data applications in science and business that require the analysis and management of huge amounts of graph data. The systems that are suitable to analyze and manage such graph data need to meet an increasing number of challenging requirements such as powerful query and graph mining capabilities, support for an expressive graph data model with heterogeneous vertices and edges, easiness of use as well as scalability and high performance capabilities.

There are many different approaches to tackle those challenges for management and analysis of \enquote{big graph data}such as graph database systems, distributed graph processing systems such as Google Pregel and its variations, and graph dataflow approaches based on Apache Spark and Flink.

Recently, a new research framework has appeared, it is called Gradoop and is build on the so-called Extended Property Graph Data Model with dedicated support for analyzing not only single graphs but also collections of graphs.

\section{Do we need a supercomputer or similar? In what cases?}

Social networks, large web pages, huge databases, and many scientific problems produce large-scale graphs \cite{1742-6596-801-1-012079}. Those graphs are used for solving problems such as GPS, social media networks, network systems, traffic regulation, search engine and many more. Therefore, this need for large graphs is growing and becoming more widely used; large-scale graph processing become very interesting to analyze and important to be solved.

High performance computing (HPC) offers a scalable, cost-efficient, and somewhat easy-to-maintain resources to conduct a very large computational process. HPC systems generally take one of two paths: cluster computing, and grid computing, where a large number of dedicated computers which are connected in a very close proximity, managed in a centralized way, have a very high speed interconnect networks.

Many important graph algorithms such as graph traversal and shortest path routing are frequently used to solve most of graph problems. For example, the shortest path algorithm is an important tool to find betweenness centrality, a measure of influence in social network.

The Graph500 benchmark is the new standard benchmark for (graph) supercomputing, besides the regular Top500. This benchmark uses Breadth-first search (BFS) for searching graph's vertices. In simple words, BFS does the search by starting from a root vertex and continuing the search for their neighbor vertices until  the path is found.


Mobile devices like smart phones and tablets have become increasingly important in our daily lives due to their portability and ever-increasing computational power, which can help leveraging mobile devices for computation and analytics \cite{Chen14}. Motivated by the impressive hardware of today's mobile devices, researchers have started to investigate using mobile devices for intensive computation, for example, some research connected multiple devices together for mobile cloud computing through the help of dedicated servers and fast internet connections.

Even though mobile devices often have limited main memory, \citet{Chen14} study the leveraging of memory mapping, which is a common and familiar capability provided by all modern operating systems, in order to fit a large graph's edges into the virtual memory of the OS. This way, the graph size is no longer constrained by the main memory available on the mobile device, which is typically no more than a few gigabytes, and therefore they can keep their approach simple, robust, and powerful. They conclude that thanks to the impressive computation power of today's mobile devices, their investigation shows that an iPad mini (2014) can perform fast computation on large real graphs with as many as 272 million edges, at a speed that is comparable to a 13" Macbook Pro (2014). Based on the familiar memory mapping capability provided by today's operating systems, their approach to scale up computation is intentionally kept simple, yet powerful. 

\section{Which type of Software Stack? How are nodes and links stored?}

\subsection{Graph Processing System Architectures}

Graph processing systems can be categorized into three types of architecture models as depicted in Figure \ref{graph_processing_architectures}.

\begin{itemize}
\item \textit{Distributed Architecture}: Includes several processing units (host) and each host has access to only its own private memory. Each partition of the graph is typically assigned to one host to be processed while the hosts interact with each other by explicit or implicit message passing.

\item \textit{Shared-Memory Architecture}: A single machine consists of one processing unit (host), which can have one or more CPU cores, and physical memory that ranges from a few to hundreds of gigabytes that is shared across all the cores. Shared memory frameworks are inherently limited in the amount of memory and CPU cores present in that single machine

\item \textit{Heterogeneous Architecture}: Not every processing unit is equally powerful. This may be a single machine and additional on-board accelerators and specialized devices, or it can also consist of distributed, non-homogeneous systems. GPUs and FPGAs are examples of additional on-board accelerators and specialized devices.
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{figures/graph_processing_architectures.png}
\caption{Graph processing architectures. \citet{Heidari:2018:SGP:3212709.3199523}}
\label{graph_processing_architectures}
\end{figure}

\subsection{Graph Processing Frameworks}

Graph processing frameworks enable graphs to be processed on different infrastructures such as
clusters and clouds. Figure \ref{graph_processing_frameworks} depicts the taxonomy of graph
processing frameworks according to main characteristics of the graph and other alternatives.

\begin{itemize}
\item \textit{Vertex-Centric (Edge-Cut) Frameworks}: A vertex-centric system partitions the graph based on its vertices, and distributes the vertices across different partitions, either by hashing them without regard to their connectivity, or by trying to reduce the edge cuts across partitions.
Edges that connect vertices lying in two different partitions either form remote edges that are shared by both partitions or owned by the partition with the source vertex.

\item \textit{Edge-Centric (Vertex-Cut) Frameworks}: Edges are the primary unit of computation and partitioning, and vertices that are attached to edges lying in different partitions are replicated and shared between those partitions. It means that each edge of the graph will be assigned to one partition, but each vertex might exist in more than one partition. It is also important to create edge-balanced partitions in this method to load balance the computation across workers, just as vertex balancing is important for vertex-centric frameworks.

\item \textit{Component-Centric Frameworks}: They have been recently introduced, where components are collections of vertices and or edges that are coarser than a single vertex or edge. They divide the whole graph into partitions and assign those partitions to machines for being processed. A partition, which is a collection of vertices and edges in the graph, forms the unit of computing.
In contrast to a vertex-centric model that hides partitioning and component connectivity details from users, it exposes the partition's structure to the users to allow optimizations. So, the performance of the system depends on the partitioning strategy that is used and how effectively users exploit the access to the coarse components in their execution. On the other hand, communication within a partition is by direct memory access, which is faster than passing messages between each single vertex in a vertex-centric model.

\item \textit{Other Graph Frameworks}: In addition to the aforementioned programming abstractions, other alternatives have been developed as well. Several data-centric models offer a declarative dataflow interface to users to access and process data without needing to explicitly define communication mechanisms. For example, MapReduce provides a dataflow programming model that is popular for processing bulk on-disk data, but not for in-memory computations across multiple iterations, and applications do not have online access to the intermediate states.
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{figures/graph_processing_frameworks.png}
\caption{Graph element-based approaches for graph processing frameworks. \citet{Heidari:2018:SGP:3212709.3199523}}
\label{graph_processing_frameworks}
\end{figure}

\subsection{Native Graph Storage}

According to \citet{Robinson:2015:GDN:2846367}, if index-free adjacency is the key to high-performance traversals, queries, and writes, then one key aspect of the design of a graph database is the way in which graphs are stored. An efficient, native graph storage format supports extremely rapid traversals for arbitrary graph algorithms (an important reason for using graphs).

For illustrative purposes I will use the Neo4j database\footnote{\url{https://neo4j.com/}} as an example of how a graph database is architected.

Figure \ref{neo4j_arch} shows Neo4j’s high-level architecture.

\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{figures/neo4j_arch.png}
\caption{Neo4j architecture. \citet{Robinson:2015:GDN:2846367}}
\label{neo4j_arch}
\end{figure}

Neo4j stores graph data in a number of different store files. Each store file contains the data for a specific part of the graph (e.g., there are separate stores for nodes, relationships, labels, and properties). The division of storage responsibilities, particularly the separation of graph structure from property data, facilitates performant graph traversals, even though it means the user's view of their graph and the actual records on disk are structurally dissimilar. 

The structure of nodes and relationships on disk (physical storage) is shown in Figure \ref{neo4j_node}.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{figures/neo4j_node.png}
\caption{Neo4j node and relationship store file record structure. \citet{Robinson:2015:GDN:2846367}}
\label{neo4j_node}
\end{figure}

The node store file stores node records. Every node created in the user-level graph ends up in the node store. Like most of the Neo4j store files, the node store is a fixed-size record store, where each record is nine bytes in length. Fixed-size records enable fast lookups for nodes in the store file. If we have a node with id $100$, then we know its record begins $900$ bytes into the file. Based on this format, the database can directly compute a record's location, at cost $O(1)$, rather than performing a search, which would be cost $O(log n)$.

The first byte of a node record is the in-use flag. This tells the database whether the record is currently being used to store a node, or whether it can be reclaimed on behalf of a new node.  The next four bytes represent the ID of the first relationship connected to the node, and the following four bytes represent the ID of the first property for the node. The five bytes for labels point to the label store for this node (labels can be inlined where there are relatively few of them). The final byte \textit{extra} is reserved for flags. One such flag is used to identify densely connected nodes, and the rest of the space is reserved for future use. The node record is quite lightweight: it's really just a handful of pointers to lists of relationships, labels, and properties.

Correspondingly, relationships are stored in the relationship store file. Like the node store, the relationship store also consists of fixed-sized records. Each relationship record contains the IDs of the nodes at the start and end of the relationship, a pointer to the relationship type (which is stored in the relationship type store), pointers for the next and previous relationship records for each of the start and end nodes, and a flag indicating whether the current record is the first in what's often called the relationship chain.

To read a node's properties, we follow the singly linked list structure beginning with the pointer to the first property. To find a relationship for a node, we follow that node's relationship pointer to its first relationship. From here, we then follow the doubly linked list of relationships for that particular node (that is, either the start node doubly linked list, or the end node doubly linked list) until we find the relationship we're interested in.

Having found the record for the relationship we want, we can read that relationship's properties (if there are any) using the same singly linked list structure as is used for node properties, or we can examine the node records for the two nodes the relationship connects using its start node and end node IDs. These IDs, multiplied by the node record size, give the immediate offset of each node in the node store file.

\section{Better representations for some problems?}

\citet{Robinson:2015:GDN:2846367} conclude that graph databases give software professionals the power to represent a problem domain using a graph, and then persist and query that graph at runtime. We can use graphs to clearly describe a problem domain; graph databases then allow us to store this representation in a way that maintains high affinity between the domain and the data. Further, graph modeling removes the need to normalize and denormalize data using complex data management code.\newline

In the field of bioinformatics, graphs are ubiquitous and frequently consist of too many nodes and edges to represent in random access memory. These graphs are thus stored in databases to allow for efficient queries using declarative query languages such as Structured Query Language (SQL). Traditional relational databases (e.g. MySQL and PostgreSQL) have long been used for this purpose and are based on decades of research into query optimization \cite{theil13}.

Recently, NoSQL databases have caught a lot of attention because of their advantages in scalability. The term NoSQL is used to refer to schemaless databases such as key/value stores (e.g. Apache Cassandra), document stores (e.g. MongoDB) and graph databases (e.g. AllegroGraph, Neo4J, OpenLink Virtuoso), which do not fit within the traditional relational paradigm.

Most NoSQL databases do not have a declarative query language. The widely used Neo4J graph database is an exception. Its query language Cypher is designed for expressing graph queries, but is still evolving

To illustrate the pros and cons of using a graph database (exemplified by Neo4J v1.8.1) instead of a relational database (PostgreSQL v9.1), \citet{theil13} imported into both the human interaction network from STRING v9.05 (Franceschini et al., 2013), which is an approximately scale-free network with 20 140 proteins and 2.2 million interactions.

As all graph databases, Neo4J stores edges as direct pointers between nodes, which can thus be traversed in constant time. Because Neo4j uses the property graph model, nodes and edges can have properties associated with them; they use this for storing the protein names and the confidence scores associated with the interactions (Figure \ref{relational_vs_graph}).

In PostgreSQL, they stored the graph as an indexed table of node pairs, which can be traversed with either logarithmic or constant look up complexity depending on the type of index used. On these databases they benchmarked the speed of Cypher and SQL queries for solving three bioinformatics graph processing problems: finding immediate neighbors and their interactions, finding the best scoring path between two proteins and finding the shortest path between them.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{figures/relational_vs_graph.png}
\caption{Relational versus graph database representation of a small protein interaction network. In the relational database, the network is stored as an interactions table (left). By contrast a graph database directly stores interactions as pointers between protein nodes (right). \citet{theil13}}
\label{relational_vs_graph}
\end{figure}

Figure \ref{relational_vs_graph_benchmark_results} shows the results after running the three explained bioinformatics graph processing problems.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{figures/relational_vs_graph_benchmark_results.png}
\caption{Ran on a Linux machine equipped with a 3GHz quad-core Intel Core i3 processor, 4 GB random access memory and a 250 GB 7200 rpm hard drive. \citet{theil13}}
\label{relational_vs_graph_benchmark_results}
\end{figure}

This shows what is possible when tightly integrating efficient algorithms with graph databases.

In summary, graph databases themselves are ready for bioinformatics and can offer great speedups over relational databases on selected problems.

The fact that a certain dataset is a graph, however, does not necessarily imply that a graph database is the best choice; it depends on the exact types of queries that need to be performed.

\newpage

\bibliographystyle{unsrtnat}
\bibliography{bibliography}

\end{document}