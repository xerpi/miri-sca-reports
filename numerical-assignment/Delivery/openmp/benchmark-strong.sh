#!/bin/bash

program=cgp3d_omp

nthreads=(1 4 8 12 16 20 24)
size=200

preconds=(id as ssor)
max_it=10000

make "$program"

for precond in "${preconds[@]}"
do
	for index in "${!nthreads[@]}"
	do
		nthread="${nthreads[$index]}"
		
		export PROGRAM="$program"
		export PROG_ARGS="-p $precond -n $size -M $max_it"
		export NUM_THREADS="$nthread"
	
		echo "#Threads: $nthread, preconditioner: $precond, size: $size, max it: $max_it"
	
		qsub -sync yes -l execution benchmark-submit.sh
	done
done
